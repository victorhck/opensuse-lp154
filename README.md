# openSUSE Leap 15.4 KDE setup

Este código lo ha desarrollado (c) Niki Kovacs 2022 y está disponible en su repositorio de GitLab en este enlace:

* https://gitlab.com/kikinovak/opensuse-lp154

## Este repositorio ofrece

  * `upgrade.sh`: Un script para actualizar de una versión 15.x de openSUSE Leap a Leap 15.4.

  * `setup.sh`: Un script para ejecutar en la post instalación de openSUSE Leap 15.4 KDE. 

## Actualiza una instalación existente de openSUSE Leap

Si ya tienes funcionando una versión previa de openSUSE Leap, utiliza este script para actualizar a openSUSE Leap 15.4 siguiendo estos pasos:

  2. Instala Git (si ya lo tienes instalado en tu sistema omite este paso): `zypper install --no-recommends git`

  3. Clona el repositorio: `git clone https://gitlab.com/kikinovak/opensuse-lp154`

  1. Arranca en modo multi-user: `systemctl set-default multi-user.target && reboot`

  5. Entra en el directorio creado en el paso 2: `cd opensuse-lp154`

  6. Ejecuta el script: `./upgrade.sh`

  7. Prepárate una taza de café o saca una cerveza del frigorífico mientras el script realiza todo el trabajo.

  8. Cuando acabe el paso anterior, reinicia y comprueba si funciona correctamente el sitema gráfico: `systemctl isolate graphical.target`

  9. Arranca en el modo gráfico: `systemctl set-default graphical.target`

> Actualmente el script realiza actualizaciones desde versiones de openSUSE Leap 15.x

## El escritorio de Linux perfecto en pocos pasos

Realiza los siguientes pasos:

  1. Instala openSUSE Leap 15.4 KDE.

  2. Abre una terminal (Konsole) como usuario _root_ (`su -`).

  3. Instala Git (si ya lo tienes instalado puedes saltarte este paso): `zypper install --no-recommends git`

  4. Descarga el script y la configuración clonando el repositorio en tu equipo: `git clone https://gitlab.com/kikinovak/opensuse-lp154`

  5. Entra dentro del directorio recién creado: `cd opensuse-lp154`

  6. Ejecuta el script: `./setup.sh --setup`

  7. Prepárate una taza de café o saca una cerveza del frigorífico mientras el script realiza el trabajo.

  8. Cuando acabe el paso anterior cierra la sesión y vuelve a entrar o reinicia tu equipo.


## Personalización del escritorio Linux

Este script personaliza la tarea de dejar tu escritorio configurado sin necesidad de consumir tiempo en hacerlo. Realiza:

  * Personalización de la shell Bash: prompt, aliases, etc.

  * Personaliza el editor Vim.

  * Configura repositorios oficiales y de terceros.

  * Elimina algunos programas innecesarios.

  * Instala programas necesarios.

  * Mejora las capacidades multimedia con varios _códecs_ y complementos.

  * Instala las fuentes gráfica de Microsoft y Eurostile para una mejor interoperabilidad.

  * Edita las entradas de los menús de aplicaciones.

  * Configura el escritorio Plasma de KDE.

El script `setup.sh`  realiza todas estas operaciones y algunas más.

Configura Bash, Vim y Xterm:

```
# ./setup.sh --shell
```

Configura repositorios oficiales y de terceros:

```
# ./setup.sh --repos
```

Actualiza el sistema con paquetes mejorados y menús límpios:

```
# ./setup.sh --fresh
```

Elimina programas innecesarios:

```
# ./setup.sh --strip
```

Instala aplicaciones y herramientas extras:

```
# ./setup.sh --extra
```

Instala las fuentes tipográficas de Microsoft y Eurostile:

```
# ./setup.sh --fonts
```

Configura las entradas de los menús:

```
# ./setup.sh --menus
```

Instala un perfil de KDE personalizado:

```
# ./setup.sh --kderc
```

Aplica el perfil personalizado de KDE a los usuarios existentes:

```
# ./setup.sh --users
```

Realiza todas las tareas anteriores:

```
# ./setup.sh --setup
```

Instala aplicaciones relacionadas con DVD/RW:

```
# ./setup.sh --dvdrw
```

Muestra un resumen de todas las opciones disponibles:

```
# ./setup.sh --help
```

Si deseas ver lo que realiza mientras el script está ejecutándose, abre una segunda terminal y echa un vistazo a los _logs_

```
$ tail -f /tmp/setup.log
```

---

<p align="center"><small><em>Developed and written by <a
href="https://tinyurl.com/2p8tk495">Nicolas Kovacs</a>.<br /> Click on the cup
to buy me a coffee.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>

